package cminds.test;

public class CmindsCodingDao {	
	public static CmindsCodingDao instance=null;
	private CmindsCodingDao() {			
		
	}	
	public static CmindsCodingDao getInstance() {
		if(instance==null) {
			return new CmindsCodingDao();
		}else {
			return instance;
		}
	}
				
	public float getPrimuamValue1(CmindsBean cmindsBean)
	{
		
	  float totalpremiumValueForAge=0.00f;	  
	  float percentageForGoodHiabit = 0.0f;	
	  float percentageForCurrentHelth = 0.0f;
	  float percentageForBadHiabit = 0.0f;
	  float totalPremiumAmount=0.0f;
		
		int i=1;		
	
		try{
		 totalpremiumValueForAge=getPercentage(cmindsBean); 
		 if("Yes".equalsIgnoreCase(cmindsBean.getSmoking()) || "Yes".equalsIgnoreCase(cmindsBean.getAlcohal()) ||
					"Yes".equalsIgnoreCase(cmindsBean.getDrug())){  
			 percentageForBadHiabit = 0.03f * i;
							
			}
			if("Yes".equalsIgnoreCase(cmindsBean.getDailyExercise())) {
				percentageForGoodHiabit = 0.03f * i;
			  						
			} 
			
			if("Yes".equalsIgnoreCase(cmindsBean.getHypertension()) || "Yes".equalsIgnoreCase(cmindsBean.getBloodPressure())|| 
					"Yes".equalsIgnoreCase(cmindsBean.getBloodSugar()) || "Yes".equalsIgnoreCase(cmindsBean.getOverweight())) {
				percentageForCurrentHelth = 0.01f * i;			
										
			}
	
		 
		 float totalPercentByIndivual =totalpremiumValueForAge+percentageForGoodHiabit+percentageForCurrentHelth+percentageForBadHiabit;
		 float perOnTotalAmount = getBaseAmount() * totalPercentByIndivual;	
		 totalPremiumAmount = getBaseAmount() + perOnTotalAmount;
		
		 System.out.println("Total Premium Amount = "+totalPremiumAmount);
		}catch(Exception e){
			e.printStackTrace();
		}
			
		return totalPremiumAmount;
	}
	
	public float getPercentage(CmindsBean cmindsBean)
	{
		float returnPercentage = 0.0f;
		int percentageForMale = 0;
		
		if (cmindsBean.getGender().equalsIgnoreCase("Male"))
		{
			percentageForMale = 2;
		}
		
		try{
		if ((cmindsBean.getAge() >= 18) && (cmindsBean.getAge() < 25))
			returnPercentage = (float)(percentageForMale+10)/100;
		else if ((cmindsBean.getAge() >= 25) && (cmindsBean.getAge() < 30))
			returnPercentage = (float)(percentageForMale+20)/100;
		else if ((cmindsBean.getAge() >= 30) && (cmindsBean.getAge() < 35))
		{
			returnPercentage = (float)(percentageForMale+30)/100;
		}
		else if ((cmindsBean.getAge() >= 35) && (cmindsBean.getAge() < 40))
			returnPercentage = (float)(percentageForMale+40)/100;
		else if (cmindsBean.getAge() >=40)
			returnPercentage = (float)(percentageForMale+60)/100;
		}catch(ArithmeticException e1){
			e1.printStackTrace();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
		return returnPercentage;
	}
	public int getBaseAmount()
	{
		return 5000;
	}
	

}
